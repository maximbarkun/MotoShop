package controller;

import domain.Motorcyclist;

public interface Cost {
    double getFinalPrice(Motorcyclist motorcyclist);
}
