package domain.equipment.enums;

public enum TypeSeason {
    SUMMER,
    AUTUMN,
    SPRING,
    WINTER
}
