package domain.equipment.enums;

public enum TypeHelmet {

    INTEGRALS,
    MODULARS,
    OPEN,
    CROSS
}
