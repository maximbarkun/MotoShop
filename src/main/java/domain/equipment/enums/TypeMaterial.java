package domain.equipment.enums;

public enum TypeMaterial {
    LEATHER,
    KNITTING
}
